#pragma once

#include <map>
#include <string>
#include <algorithm>
#include <exception>
#include <boost/range.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace httpc
{

//-------------------------------------------------------------------------------------------------

struct http_response_header
{
	typedef std::string string;
	std::pair<int, string> status;
	std::map<string, string> header;

	//---------------------------------------------------------------------------------------------
	
	template <typename Range>
	size_t parse_header(const Range &r)
	{
		namespace phx = boost::phoenix;
		namespace qi = boost::spirit::qi;
		
		typedef BOOST_DEDUCED_TYPENAME boost::range_iterator<const Range>::type iterator;
		
		iterator ite = boost::begin(r);
		const iterator end = boost::end(r);

		typedef std::pair<string, string> pair_type;

		const qi::rule<iterator, pair_type()> header_line((qi::as_string[+(qi::char_ - '\r' - ':')] > ':' > *qi::lit(' ') > qi::as_string[+(qi::char_ - '\r')] > "\r\n")[qi::_val = phx::construct<pair_type>(qi::_1, qi::_2)]);
		qi::parse(ite, end, ("HTTP/" > qi::digit > '.' > qi::digit > ' ' > qi::int_[phx::ref(status.first) = qi::_1] > ' ' > qi::as_string[+(qi::char_ - '\r')][phx::ref(status.second) = qi::_1] > "\r\n"));
		qi::parse(ite, end, *(header_line)[phx::insert(phx::ref(header), qi::_1)] > "\r\n");

		if (ite != end)
			throw std::runtime_error("invalid header");

		return boost::size(r);
	}

	//---------------------------------------------------------------------------------------------
};

//-------------------------------------------------------------------------------------------------

struct plain_response : http_response_header
{
	template <typename Range>
	size_t append_data_body(const Range &r)
	{
		boost::copy(r, std::ostream_iterator<char>(std::cout));
		std::cout << "\n******" << std::endl;
		return boost::size(r);
	}
};

//-------------------------------------------------------------------------------------------------

namespace detail
{

//-------------------------------------------------------------------------------------------------

template <typename Range>
inline size_t parse_chunk_size(const Range &r, size_t &s)
{
	namespace phx = boost::phoenix;
	namespace qi = boost::spirit::qi;
	
	typedef BOOST_DEDUCED_TYPENAME boost::range_iterator<const Range>::type iterator;
	
	iterator ite = boost::begin(r);
	const iterator end = boost::end(r);

	qi::parse(ite, end, (qi::hex[phx::ref(s) = qi::_1] >> +(qi::char_ - '\r') > "\r\n"));
	
	return std::distance(ite, end);
}

//-------------------------------------------------------------------------------------------------

} // end namespace detail
} // end namespace httpc
