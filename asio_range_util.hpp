#pragma once

#include <boost/range/iterator_range.hpp>
#include <boost/range/algorithm/search.hpp>
#include <boost/asio/buffer.hpp>

namespace httpc
{

//-------------------------------------------------------------------------------------------------
	
template <typename T>
inline boost::iterator_range<T*> make_iterator_range_from_memory(T *head, std::size_t size)
{
	return boost::make_iterator_range(head, head + size);
}

//-------------------------------------------------------------------------------------------------

template <typename Ptr>
inline boost::iterator_range<Ptr> make_iterator_range_from_buffer(const boost::asio::const_buffer &b)
{
	return make_iterator_range_from_memory(boost::asio::buffer_cast<Ptr>(b),
										   boost::asio::buffer_size(b));
}

//-------------------------------------------------------------------------------------------------

struct lazy_range_search_t
{
	template <typename R, typename S>
	struct result
	{
		typedef BOOST_DEDUCED_TYPENAME boost::range_iterator<R>::type type;		
	};
	
	template <typename R, typename S>
	BOOST_DEDUCED_TYPENAME result<R, S>::type operator()(const R &r, const S &s) const
	{
		return boost::search(r, s);
	}
};

//-------------------------------------------------------------------------------------------------

template <typename R, typename EndFinder>
inline R make_partial_range(R r, const EndFinder &fn)
{
	return R(boost::begin(r), fn(r));	
}

//-------------------------------------------------------------------------------------------------

} // end namespace HttpClient
