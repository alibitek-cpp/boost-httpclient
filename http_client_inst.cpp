#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/karma.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/range.hpp>
#include <boost/range/adaptor/transformed.hpp>

#include "http_connection.hpp"
#include "asio_range_util.hpp"
#include <boost/bind.hpp>
#include "http_client.hpp"

namespace httpc
{

//-------------------------------------------------------------------------------------------------
	
namespace
{

//-------------------------------------------------------------------------------------------------

template <typename Range>
inline std::size_t parse_chunk_size(const Range &r, std::size_t &s)
{
	namespace phx = boost::phoenix;
	namespace qi = boost::spirit::qi;
	
	typename boost::range_iterator<const Range>::type ite = boost::begin(r);
	qi::parse(ite, boost::end(r), (qi::hex[phx::ref(s) = qi::_1] > *(qi::char_ - '\n') > '\n'));
	
	return std::distance(boost::begin(r), ite);
}

//-------------------------------------------------------------------------------------------------

template <typename Range>
std::size_t parse_header(const Range &r, http_response_data &res)
{
	namespace phx = boost::phoenix;
	namespace qi = boost::spirit::qi;
	typedef typename boost::range_iterator<const Range>::type iterator;
	
	iterator ite = boost::begin(r);
	const iterator end = boost::end(r);
	typedef std::pair<std::string, std::string> pair_type;
	
	qi::parse(ite, end,
	          ("HTTP/" > qi::digit > '.' > qi::digit > ' ' > qi::int_[phx::ref(res.status.first) = qi::_1]
	           > ' ' > qi::as_string[+(qi::char_ - '\r')][phx::ref(res.status.second) = qi::_1] > "\r\n"));
	
	qi::parse(ite, end,
	          *((qi::as_string[+(qi::char_ - '\r' - ':')] > ": " > qi::as_string[+(qi::char_ - '\r')] > "\r\n")
	            [phx::insert(phx::ref(res.header), phx::construct<pair_type>(qi::_1, qi::_2))]) > "\r\n");
	
	return std::distance(boost::begin(r), ite);
}

//-------------------------------------------------------------------------------------------------

const std::string eol1 = "\r\n";
const std::string eol2 = "\r\n\r\n";

}

//-------------------------------------------------------------------------------------------------

std::map<std::string, std::string> split_params(const std::string &data)
{
	namespace phx = boost::phoenix;
	namespace qi = boost::spirit::qi;
	std::string::const_iterator ite = data.begin();
	std::map<std::string, std::string> r;
	qi::parse(ite, data.end(), *((qi::as_string[+(qi::char_ - '=')] >> '=' >>
qi::as_string[+(qi::char_ - '&')] >> '&')
	                             [phx::insert(phx::ref(r), phx::construct<std::pair<std::string,
std::string> >(qi::_1, qi::_2))]));
	return r;
}

//-------------------------------------------------------------------------------------------------

http_client::http_client(boost::asio::io_service &serv, http_handler handler, const std::string
&host)
	: host(host), path(), query(), header(),
	  con(serv, *resolver_type(serv).resolve(resolver_type::query(host, "http"))), sb(),
handler(handler) { }

//-------------------------------------------------------------------------------------------------
	  
http_client::http_client(boost::asio::io_service &serv, http_handler handler, const
resolver_type::endpoint_type &host)
	: host(host.address().to_string()), path(), query(), header(), con(serv, host), sb(),
handler(handler) { }

//-------------------------------------------------------------------------------------------------
	
http_client::http_client(boost::asio::io_service &serv, http_handler handler, const std::string
&host, boost::asio::ssl::context &ctx)
	: host(host), path(), query(), header(),
	  con(serv, *resolver_type(serv).resolve(resolver_type::query(host, "https")), ctx), sb(),
handler(handler) { }
	  
//-------------------------------------------------------------------------------------------------
	  
http_client::http_client(boost::asio::io_service &serv, http_handler handler, const
resolver_type::endpoint_type &host, boost::asio::ssl::context &ctx)
	: host(host.address().to_string()), path(), query(), header(), con(serv, host, ctx), sb(),
handler(handler) { }

//-------------------------------------------------------------------------------------------------

static std::string DecompressGzipData(const std::string& compressedString)
{
	std::istringstream src(compressedString);
	
	if (src.good())
	{
		boost::iostreams::filtering_istreambuf in;
		in.push(boost::iostreams::gzip_decompressor());
		in.push(src);
		
		std::ostringstream dst;
		boost::iostreams::copy(in, dst);
		
		return dst.str();
	}
	
	return "";
}

void http_client::start_read()
{
	boost::shared_ptr<http_response_data> d(boost::make_shared<http_response_data>());
	boost::asio::async_read_until(con.get_stream(), sb, "\r\n\r\n",
				boost::bind(&this_type::header_read_complete, shared_from_this(), d, _1, _2));
}

//-------------------------------------------------------------------------------------------------

void http_client::header_read_complete(boost::shared_ptr<http_response_data> res,
									   const error_code &ec,
									   std::size_t len)
{
	std::clog << "Length: " << len << std::endl;
	
	if (sb.size() == 0) return;
	
	sb.consume(parse_header(make_iterator_range_from_buffer<const char *>(sb.data()), *res));
	const std::string trn_enc = util::map_get_or(res->header, "Transfer-Encoding", "");

#ifdef _DEBUG
	std::clog << "[HTTP response header]" << std::endl;
	for (const auto& header : res->header)
	{
		std::clog << header.first << ":" << header.second << std::endl;
	}
#endif

	if (trn_enc == "chunked") // Transfer-Encoding : chunked 
	{
		const std::string encoding = util::map_get_or(res->header, "Content-Encoding", "");
		if (encoding == "gzip")
		{
			std::clog << "__gzip__" << std::endl;

// 			sb.prepare();
// 			boost::asio::mutable_buffers_1 buffer();
// 			socket_stream_wrapper::mutable_buffer mutableBuffer = sb.prepare(4096);
// 			con.get_stream().async_read_some(mutableBuffer,
// 											 [this, res] (const error_code& ec, std::size_t size)
// 			{
// 				res->data.assign(boost::asio::buffer_cast<const char *>(sb.data()), size);
// 				sb.consume(res->data.length());
// 				handler(shared_from_this(), res);
// 			});

			boost::asio::async_read_until(con.get_stream(), sb, eol1,
										  bind(&this_type::chunk_size_read_complete, shared_from_this(), res, _1, _2));
		}
		else if (encoding == "deflate")
		{
			std::clog << "__deflate__" << std::endl;
		}
		else
		{
			boost::asio::async_read_until(con.get_stream(), sb, eol1,
										  bind(&this_type::chunk_size_read_complete, shared_from_this(), res, _1, _2));
		}
	} 
	else // Content-Length : <value>
	{
		const std::size_t length = boost::lexical_cast<std::size_t>(util::map_get_or(res->header,
																	"Content-Length", "0"));
		boost::asio::async_read(con.get_stream(), sb,
								boost::asio::transfer_at_least(length - boost::asio::buffer_size(sb.data())),
								bind(&this_type::plain_body_read_complete, shared_from_this(), res,
									 length, _1, _2));
	}
}

//-------------------------------------------------------------------------------------------------

void http_client::plain_body_read_complete(boost::shared_ptr<http_response_data> res, std::size_t
length, const error_code &ec, std::size_t len)
{
	if (sb.size() == 0) return;

	res->data.assign(boost::asio::buffer_cast<const char *>(sb.data()), length);
	sb.consume(res->data.length());
	handler(shared_from_this(), res);
	start_read();
}

//-------------------------------------------------------------------------------------------------

void http_client::chunk_size_read_complete(boost::shared_ptr<http_response_data> res,
										   const error_code &ec, std::size_t len)
{
	if (sb.size() == 0) return;

	if (sb.size() <= 2)
	{
		boost::asio::async_read_until(con.get_stream(), sb, eol1,
				bind(&this_type::chunk_size_read_complete, shared_from_this(), res, _1, _2));
	}
	else
	{
		std::size_t chunk_size = 0;
		sb.consume(parse_chunk_size(make_iterator_range_from_buffer<const char *>(sb.data()), chunk_size));
		const std::size_t bufsize = boost::asio::buffer_size(sb.data());

		if (chunk_size + 2 > bufsize)
		{
			boost::asio::async_read(con.get_stream(), sb,
									boost::asio::transfer_at_least(chunk_size + 2 - bufsize),
			                 bind(&this_type::chunk_body_read_complete, shared_from_this(), res, chunk_size, _1, _2));
		}
		else
		{
			chunk_body_read_complete(res, chunk_size, ec, 0);
		}
	}
}

//-------------------------------------------------------------------------------------------------

void http_client::chunk_body_read_complete(boost::shared_ptr<http_response_data> res, std::size_t chunk_size,
										   const error_code &ec, std::size_t len)
{
	if (sb.size() == 0) return;

	if (chunk_size == 0)
	{
		boost::asio::async_read_until(con.get_stream(), sb, eol2,
				bind(&this_type::lastchunk_read_complete, shared_from_this(), _1, _2));
	}
	else
	{
		assert(boost::asio::buffer_size(sb.data()) >= chunk_size + 2);
		
		res->data.assign(boost::asio::buffer_cast<const char *>(sb.data()), chunk_size + 2);
		sb.consume(res->data.length());

		std::cout << res->data;
		
		handler(shared_from_this(), res);
		
		boost::asio::async_read_until(con.get_stream(), sb, eol1,
							bind(&this_type::chunk_size_read_complete, shared_from_this(), res, _1, _2));
	}
}

//-------------------------------------------------------------------------------------------------

void http_client::lastchunk_read_complete(const error_code &ec, std::size_t)
{
	if (sb.size() == 0) return;

	start_read();
}

//-------------------------------------------------------------------------------------------------

void http_client::write_complete(const boost::shared_ptr<std::string> &,
								 const error_code &, std::size_t)
{
}

//-------------------------------------------------------------------------------------------------

void http_client::send_request(const std::string &data)
{
	boost::shared_ptr<std::string> pdata(boost::make_shared<std::string>(generate_http_header(data)
																		+ data));
#ifdef _DEBUG
	std::cout << std::endl << "[HTTP Request]" << std::endl << *pdata << std::endl;
#endif
	
	boost::asio::async_write(con.get_stream(), boost::asio::buffer(*pdata),
							 bind(&this_type::write_complete, shared_from_this(), pdata, _1, _2));
}

//-------------------------------------------------------------------------------------------------

std::string http_client::generate_http_header(const std::string &data) const
{
	namespace karma = boost::spirit::karma;
	
	std::string s = (data.length() ? "POST " : "GET ") + path + (query.empty() ? "" : "?")
					+ query + " HTTP/1.1\r\n";

	s += "Host: " + host + "\r\n";

	if (data.length())
	{
		s += "Content-Type: application/x-www-form-urlencoded;charset=UTF-8\r\n";
	}	

	karma::generate(
	  back_inserter(s),
	  *(karma::string << ": " << karma::string << "\r\n")
	  << (&karma::uint_(0) | ("Content-Length: " << karma::uint_ << "\r\n"))
	   << "\r\n", header, data.length());
	
	return s;
}

//-------------------------------------------------------------------------------------------------

}
