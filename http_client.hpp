#pragma once

#include <boost/lexical_cast.hpp>
#include <boost/functional.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/make_shared.hpp>

#include <string>

#include "util.hpp"
#include "http_connection.hpp"

namespace httpc
{
	
//-------------------------------------------------------------------------------------------------

struct http_response_data
{
	std::pair<int, std::string> status;
	std::map<std::string, std::string> header;
	std::string data;
};

//-------------------------------------------------------------------------------------------------

class http_client;

//-------------------------------------------------------------------------------------------------

typedef boost::function <void (const boost::shared_ptr<http_client> &cli,
							   const boost::shared_ptr<http_response_data> &data)> http_handler;

//-------------------------------------------------------------------------------------------------
						 
std::map<std::string, std::string> split_params(const std::string& data);

//-------------------------------------------------------------------------------------------------

class http_client : public boost::enable_shared_from_this<http_client>
{
	typedef http_client this_type;
	
public:
	typedef boost::asio::ip::tcp::resolver resolver_type;
	
public:
	http_client(boost::asio::io_service& serv, http_handler handler, const std::string& host);
	
	http_client(boost::asio::io_service& serv, http_handler handler, const resolver_type::endpoint_type& host);
	
	http_client(boost::asio::io_service& serv, http_handler handler, const std::string& host,
				boost::asio::ssl::context &ctx);
	
	http_client(boost::asio::io_service& serv, http_handler handler,
				const resolver_type::endpoint_type& host, boost::asio::ssl::context& ctx);
	
	std::string get_uri() const { return get_schema_string() + "://" + host + path; }
	
	void get() { send_request(std::string()); }
	
	void post(const std::string& data) { send_request(data); }
	
	void start_read();
	
	boost::shared_ptr<http_client> get_shared() { return shared_from_this(); }
	
	void abort() { error_code ec; con.get_stream().close(ec); }
	
	bool connected() const { return con.get_stream().is_open(); }
	
public:
	const std::string host;
	std::string path;
	std::string query;
	std::map<std::string, std::string> header;
	
private:
	std::string generate_http_header(const std::string& data) const;
	
	void header_read_complete(boost::shared_ptr<http_response_data> res,
							  const error_code& ec, size_t);
	
	void plain_body_read_complete(boost::shared_ptr<http_response_data> res, size_t length,
								  const error_code &ec, size_t);
	
	void chunk_size_read_complete(boost::shared_ptr<http_response_data> res, const error_code& ec, size_t);
	
	void chunk_body_read_complete(boost::shared_ptr<http_response_data> res, size_t chunk_size,
								  const error_code& ec, size_t);
	
	void lastchunk_read_complete(const error_code& ec, size_t);
	
	void write_complete(const boost::shared_ptr<std::string> &data, const error_code& ec, size_t);
	
	void send_request(const std::string& data);
	
	std::string get_schema_string() const { return con.get_context_ptr() ? "https" : "http"; }
	
private:
	connection con;
	boost::asio::streambuf sb;
	http_handler handler;
};

//-------------------------------------------------------------------------------------------------

inline boost::shared_ptr<http_client> make_http_client(boost::asio::io_service& serv,
													   const http_handler& handler,
													   const std::string& host)
{
	return boost::make_shared<http_client>(boost::ref(serv), handler, host);
}

//-------------------------------------------------------------------------------------------------

inline boost::shared_ptr<http_client> make_http_client(boost::asio::io_service& serv,
													   const http_handler& handler,
											const http_client::resolver_type::endpoint_type& host)
{
	return boost::make_shared<http_client>(boost::ref(serv), handler, host);
}

//-------------------------------------------------------------------------------------------------

inline boost::shared_ptr<http_client> make_http_client(boost::asio::io_service& serv,
													   const http_handler& handler,
													   const std::string& host,
													   boost::asio::ssl::context& ctx)
{
	return boost::make_shared<http_client>(boost::ref(serv), handler, host, boost::ref(ctx));
}

//-------------------------------------------------------------------------------------------------

inline boost::shared_ptr<http_client> make_http_client(boost::asio::io_service& serv,
													   const http_handler& handler,
											  const http_client::resolver_type::endpoint_type& host,
											  boost::asio::ssl::context& ctx)
{
	return boost::make_shared<http_client>(boost::ref(serv), handler, host, boost::ref(ctx));
}

//-------------------------------------------------------------------------------------------------

} // end namespace httpc
